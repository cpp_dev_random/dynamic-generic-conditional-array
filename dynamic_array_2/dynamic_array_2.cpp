#include "stdafx.h"
#include <iostream>
#include <string>
#include <initializer_list>
#include "custom_array.h"
#include <type_traits>
#include <typeinfo>
#include <utility>

using namespace std;


class Generic_T
{
private:
	void* objAddr;

	void clearIfNotFree()
	{
		if (objAddr)
		{
			delete objAddr;
		}
	}

public:
	Generic_T() : objAddr(nullptr)
	{

	}

	Generic_T(const Generic_T& obj) : Generic_T() // copy assi ctor
	{
		objAddr = obj.objAddr;
	}

	Generic_T(Generic_T&& obj) : Generic_T() // move assi ctor
	{
		objAddr = obj.objAddr;

		obj.objAddr = nullptr;
	}

	template <typename T>
	Generic_T(const T& obj) : objAddr(new T)
	{
		*(T*)objAddr = obj;
	}

	~Generic_T()
	{
		clearIfNotFree();
	}

	template <typename T>
	T& getObj()
	{
		return *(T*)objAddr;
	}

	template <typename T>
	Generic_T& operator= (const T& obj)
	{
		clearIfNotFree();

		objAddr = new T;
		*(T*)objAddr = obj;
		
		return *this;
	}

	Generic_T& operator= (const Generic_T& obj) // copy assi opr // TODO: how tf to impl this ?!
	{
		clearIfNotFree();

		objAddr = obj.objAddr;

		return *this;
	}

	Generic_T& operator= (Generic_T&& obj) // move assi opr
	{
		clearIfNotFree();

		objAddr = obj.objAddr;
		
		obj.objAddr = nullptr;

		return *this;
	}
	
	template <typename T>
	operator T&()
	{
		return *(T*)objAddr;
	}
};

class MyClass // custom class for test purposes
{
private:
	int x;

public:
	MyClass()
	{
		cout << "ctor, hi!" << endl;
	}

	MyClass(int n) : x(n)
	{
		cout << "ctor, hi!" << endl;
	}

	~MyClass()
	{
		cout << "dtor, bye!" << endl;
	}

	MyClass& operator= (const MyClass& mc) // copy opr
	{
		x = mc.x;

		return *this;
	}

	MyClass& operator= (MyClass&& mc) // move opr
	{
		x = mc.x;

		mc.x = 0;

		return *this;
	}

	const int& getX() const
	{
		return x;
	}

	void printX()
	{
		cout << x << endl;
	}
};


int main(int argc, wchar_t* argv[])
{

	CustomArray<MyClass> sss1 = { 5, 3 };
	/*cout << sss1 << endl;
	const CustomArray<int> sss = move(sss1);
	cout << sss1 << endl;*/

	const MyClass mcObj(-13);					// custom class obj
	mcObj.getX();

	MyClass mcObj2(22);
	mcObj2 = std::move(mcObj);
	mcObj.getX();


	Generic_T gt1 = string("asd");		// <string> obj
	Generic_T gt2 = 123;				// <int> obj
	Generic_T gt3 = 123.45f;			// <float> obj
	Generic_T gt4 = 123.45;				// <double> obj
	Generic_T gt5 = mcObj;				// <custom class> obj

	Generic_T gt6;			// <custom class> obj
	gt6 = 123;				// <custom class> obj
	gt6 = 123;				// <custom class> obj
	gt6 = 123;				// <custom class> obj

	cout << (string)gt1				<< endl
		 << (int)gt2				<< endl
		 << (float)gt3				<< endl
		 << (double)gt4				<< endl
		 << ((MyClass)gt5).getX()	<< endl;

	CustomArray<Generic_T> cg_array = { 123, -13.47f, string("hi!"), mcObj };

	cout << (int)cg_array[0] << endl
		 << (float)cg_array[1] << endl
		 << (string)cg_array[2] << endl
		 << ((MyClass)cg_array[3]).getX() << endl;


	gt1.getObj<string>();

	int xi = 3;
	int xi2 = 3;
	float xf = 2.3f;
	float xf2 = 2.3f;

	const type_info& tid = typeid(xf);

	

	cout << (decltype("string"))(xf2) << endl;

		
	{
		//////////////////////////////////////// array of arrays (2D array)
		CustomArray<CustomArray<double>> tst1 = // IT WORKS!
		{
			{ 1.1, 2.2, 3.3 },
			{ 4.4, 5.5, 6.6 }
		};
		cout << tst1 << endl;


		tst1.push( { -13, -12, -11 } ); // push a complete array
		cout << tst1 << endl;

		tst1.pop(); // pop the last array
		cout << tst1 << endl;

		cout << tst1[1][1] << endl; // 5.5 (second array, second element)
		/////////////////////////////////////////

		///////////////////////////////////////// testing push and pop
		CustomArray<int> tst2 = { 5, 4, 3, 2, 1 };

		tst2.push(-999);
		cout << tst2 << endl;

		tst2.pop();
		cout << tst2 << endl;
		/////////////////////////////////////////

		///////////////////////////////////////// TODO: fix this
		CustomArray<int> tst3 = { 1, 2, 3 };
		CustomArray<int> tst4 = { 4, 5, 6 };
		//tst3 == tst4; // ERROR: no opr== takes LHS of type 'CustomArray' (should be RHS but MSVC is probably dumb)
		// here's the fix:
		/*
		bool operator== (const CustomArray<DataType>& d1, const CustomArray<DataType>& d2)
		{
			return true;
		}
		*/
		/////////////////////////////////////////

		///////////////////////////////////////// testing conditional assignment
		CustomArray<double> tst6 = { 1.1, 3.2, 6.7, 2.1, 0.15, 9.78, 4.57 };
		cout << tst6 << endl;

		tst6((tst6 > 0.9) & (tst6 < 8.4)) = 0;
		cout << tst6 << endl;
		/////////////////////////////////////////

	}

	return 0;
}

