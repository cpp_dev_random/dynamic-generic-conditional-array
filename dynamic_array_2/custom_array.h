#if !defined CUSTOM_DYNAMIC_ARRAY_H
#define CUSTOM_DYNAMIC_ARRAY_H


#include <cassert>

template <class DataType>
class CustomArray;

template <class DataType>
class CustomArray
{
private:
	/**** too lazy to write xD ****/
	typedef unsigned char uchar;
	typedef unsigned int uint;
	typedef unsigned long long ullong;
	/******************************/
	
	/**** allocation information ****/
	DataType* allocAddr; // address of allocated mem block
	uint allocLength; // array length
	uint allocSize; // array size (bytes)
	/********************************/
	
	void clearAllocInfo();

	class boolIDX;
	class addrIDX;

	class boolIDX
	{
	private:
		// only 'DataType' variants of 'CustomArray' may access 'objFlag' in 'addrIDX operator(boolIDX)'
		friend class CustomArray<DataType>;

		bool* objFlag;
		uint flagsLength;

	public:
		boolIDX();
		explicit boolIDX(uint len); // initialization ctor (should be used when instantiating)
		                            // explicit is not important, because there's no 'operator=' overload
		boolIDX(boolIDX&& objFlag_); // move ctor (used when returning boolIDX)

		~boolIDX();

		const boolIDX& operator& (const boolIDX& objFlag_) const;
		const boolIDX& operator| (const boolIDX& objFlag_) const;
		const boolIDX& operator~ () const;
		const boolIDX& operator^ (const boolIDX& objFlag_) const;

		uint length() const;

		friend std::ostream& operator<< (std::ostream& oStream, const boolIDX& objFlag_)
		{
			if (!(objFlag_.objFlag))
				return oStream;

			bool wasTrue = false;
			for (unsigned int i = 1; i < objFlag_.flagsLength; i++)
			{
				if (objFlag_.objFlag[i])
				{
					if (wasTrue)
						oStream << ", ";
					oStream << i;

					wasTrue = true;
				}
				
			}

			return oStream;
		}
	};

	class addrIDX
	{
	private:
		// only 'DataType' variants of 'CustomArray' may access 'objAddr' in 'addrIDX operator(boolIDX)'
		// i.e only 'CustomArray<int>' may access 'CustomArray<int>::addrIDX', and 'CustomArray<double>' is NOT allowed
		// this decreases probability of illegal access to addresses of different type
		friend class CustomArray<DataType>;

		DataType** objAddr; // addresses buffer (array of addresses)
		uint addrsLength;

	public:
		addrIDX();
		explicit addrIDX(uint len); // initialization ctor (should be used when instantiating)
									// 'explicit' is needed because the compiler confuses operator=(uint) with operator=(addrIDX(uint)) (ambiguous call)
									// ---> addrIDX = addrIDX(8) vs. addrIDX = 8 (implicit conversion is allowed)
									// so 'uint' must be distinguishable from 'addrIDX(uint)'
		addrIDX(addrIDX&& objAddr_); // move ctor (used when returning addrIDX)

		~addrIDX();

		const addrIDX& operator= (const DataType& data) const;
		const addrIDX& operator+= (const DataType& data) const;
		const addrIDX& operator-= (const DataType& data) const;
		const addrIDX& operator*= (const DataType& data) const;
		const addrIDX& operator/= (const DataType& data) const;
		const addrIDX& operator%= (const DataType& data) const;
		const addrIDX& operator>>= (const DataType& data) const;
		const addrIDX& operator<<= (const DataType& data) const;

		uint length() const;

		friend std::ostream& operator<< (std::ostream& oStream, const addrIDX& objAddr_)
		{
			if (!(objAddr_.objAddr))
				return oStream;

			oStream << *(objAddr_.objAddr[0]);
			for (unsigned int i = 1; i < objAddr_.addrsLength; i++)
				oStream << ", " << *(objAddr_.objAddr[i]);

			return oStream;
		}
	};


public:
	/**** constructors ****/
	CustomArray();
	CustomArray(uint length);
	CustomArray(const CustomArray<DataType>& csArr); // copy ctor
	CustomArray(CustomArray<DataType>&& csArr); // move ctor
	CustomArray(const std::initializer_list<DataType>& initList);
	/**********************/

	/**** destructor ****/
	~CustomArray();
	/********************/

	/**** reallocation / clearing functions ****/
	bool resize(uint newLength);
	void clear();
	/*******************************************/

	/**** getters + setters ****/
	DataType& operator[] (uint location) const;
	DataType& at(uint location) const;
	DataType& operator() (uint location) const;
	/***************************/

	/**** setters ****/
	CustomArray<DataType>& operator= (const DataType& data);
	CustomArray<DataType>& operator= (const CustomArray<DataType>& csArr); // copy assignment operator
	CustomArray<DataType>& operator= (CustomArray<DataType>&& csArr); // move assignment operator
	CustomArray<DataType>& operator= (const std::initializer_list<DataType>& initList);
	void fillWith(const DataType& data) const;

	bool copyFrom(const CustomArray<DataType>& csArr, uint srcStartIdx = 0, uint destStartIdx = 0);
	bool copyTo(CustomArray<DataType>& csArr, uint srcStartIdx = 0, uint destStartIdx = 0) const;

	bool push(const DataType& data);
	bool pop();
	/*****************/

	/**** getters ****/
	uint length() const;
	uint memSize() const;

	addrIDX operator() (const boolIDX& objFlag_) const;
	addrIDX operator() (const std::initializer_list<uint>& initList) const;

	boolIDX operator< (const DataType& data) const;
	boolIDX operator<= (const DataType& data) const;
	boolIDX operator> (const DataType& data) const;
	boolIDX operator>= (const DataType& data) const;
	boolIDX operator== (const DataType& data) const;
	boolIDX operator!= (const DataType& data) const;

	friend std::ostream& operator<< (std::ostream& oStream, const CustomArray& csArr)
	{
		if (!csArr.allocAddr)
			return oStream;

		oStream << csArr.allocAddr[0];
		for (uint i = 1; i < csArr.allocLength; i++)
			oStream << ", " << csArr.allocAddr[i];

		return oStream;
	}
	/*****************/

};

/************************************************************ implementation ************************************************************/
/****************************************************************************************************************************************/

template <class DataType>
/// <summary>clear (reset) allocation info</summary>
void CustomArray<DataType>::clearAllocInfo()
{
	allocAddr = nullptr;
	allocLength = 0;
	allocSize = 0;
}

/*--------------------------------------------------------- class boolIDX ---------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------*/
template <class DataType>
CustomArray<DataType>::boolIDX::boolIDX() : objFlag(nullptr), flagsLength(0)
{

}

template <class DataType>
CustomArray<DataType>::boolIDX::boolIDX(uint len) : objFlag(new bool[len]), flagsLength(len)
{
	for (uint i = 0; i < len; i++)
		objFlag[i] = false;
}

template <class DataType>
CustomArray<DataType>::boolIDX::boolIDX(boolIDX&& objFlag_) : objFlag(objFlag_.objFlag), flagsLength(objFlag_.flagsLength)
{
	objFlag_.objFlag = nullptr;
	objFlag_.flagsLength = 0;
}

template <class DataType>
CustomArray<DataType>::boolIDX::~boolIDX()
{
	if (objFlag)
	{
		delete[] objFlag;
		objFlag = nullptr;
		flagsLength = 0;
	}
}

template <class DataType>
const typename CustomArray<DataType>::boolIDX& CustomArray<DataType>::boolIDX::operator& (const boolIDX& objFlag_) const
{
	if (!(objFlag_.objFlag) || !objFlag) // if empty array
		return *this;

	if (objFlag_.flagsLength != flagsLength) // if mismatching lengths
		return *this;

	for (size_t i = 0; i < flagsLength; i++)
		objFlag[i] &= objFlag_.objFlag[i];

	return *this;
}

template <class DataType>
const typename CustomArray<DataType>::boolIDX& CustomArray<DataType>::boolIDX::operator| (const boolIDX& objFlag_) const
{
	if (!(objFlag_.objFlag) || !objFlag) // if empty array
		return *this;

	if (objFlag_.flagsLength != flagsLength) // if mismatching lengths
		return *this;

	for (size_t i = 0; i < flagsLength; i++)
		objFlag[i] |= objFlag_.objFlag[i];

	return *this;
}

template <class DataType>
const typename CustomArray<DataType>::boolIDX& CustomArray<DataType>::boolIDX::operator~ () const
{
	if (!objFlag) // if empty array
		return *this;

	for (size_t i = 0; i < flagsLength; i++)
		objFlag[i] = !objFlag[i];

	return *this;
}

template <class DataType>
const typename CustomArray<DataType>::boolIDX& CustomArray<DataType>::boolIDX::operator^ (const boolIDX& objFlag_) const
{
	if (!(objFlag_.objFlag) || !objFlag) // if empty array
		return *this;

	if (objFlag_.flagsLength != flagsLength) // if mismatching lengths
		return *this;

	for (size_t i = 0; i < flagsLength; i++)
		objFlag[i] ^= objFlag_.objFlag[i];

	return *this;
}

template <class DataType>
/// <summary>returns the number of matches</summary>
typename CustomArray<DataType>::uint CustomArray<DataType>::boolIDX::length() const
{
	uint len = 0;
	for (uint i = 0; i < flagsLength; i++)
	{
		if (objFlag[i])
			len += 1;
	}
	
	return len;
}
/*---------------------------------------------------------------------------------------------------------------------------------*/

/*--------------------------------------------------------- class addrIDX ---------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------*/
template <class DataType>
CustomArray<DataType>::addrIDX::addrIDX() : objAddr(nullptr), addrsLength(0)
{
	
}

template <class DataType>
CustomArray<DataType>::addrIDX::addrIDX(uint len) : objAddr(new DataType*[len]), addrsLength(len)
{
	for (uint i = 0; i < len; i++)
		objAddr[i] = nullptr;
}

template <class DataType>
CustomArray<DataType>::addrIDX::addrIDX(addrIDX&& objAddr_) : objAddr(objAddr_.objAddr), addrsLength(objAddr_.addrsLength)
{
	objAddr_.objAddr = nullptr;
	objAddr_.addrsLength = 0;
}

template <class DataType>
CustomArray<DataType>::addrIDX::~addrIDX()
{
	if (objAddr)
	{
		delete[] objAddr;
		objAddr = nullptr;
		addrsLength = 0;
	}
}

template <class DataType>
const typename CustomArray<DataType>::addrIDX& CustomArray<DataType>::addrIDX::operator= (const DataType& data) const
{
	if (!objAddr)
		return *this;

	for (uint i = 0; i < addrsLength; i++)
		*(objAddr[i]) = data;

	return *this;
}

template <class DataType>
const typename CustomArray<DataType>::addrIDX& CustomArray<DataType>::addrIDX::operator += (const DataType& data) const
{
	if (!objAddr)
		return *this;

	for (uint i = 0; i < addrsLength; i++)
		*(objAddr[i]) += data;

	return *this;
}

template <class DataType>
const typename CustomArray<DataType>::addrIDX& CustomArray<DataType>::addrIDX::operator-= (const DataType& data) const
{
	if (!objAddr)
		return *this;

	for (uint i = 0; i < addrsLength; i++)
		*(objAddr[i]) -= data;

	return *this;
}

template <class DataType>
const typename CustomArray<DataType>::addrIDX& CustomArray<DataType>::addrIDX::operator*= (const DataType& data) const
{
	if (!objAddr)
		return *this;

	for (uint i = 0; i < addrsLength; i++)
		*(objAddr[i]) *= data;

	return *this;
}

template <class DataType>
const typename CustomArray<DataType>::addrIDX& CustomArray<DataType>::addrIDX::operator/= (const DataType& data) const
{
	if (!objAddr)
		return *this;

	for (uint i = 0; i < addrsLength; i++)
		*(objAddr[i]) /= data;

	return *this;
}

template <class DataType>
const typename CustomArray<DataType>::addrIDX& CustomArray<DataType>::addrIDX::operator%= (const DataType& data) const
{
	if (!objAddr)
		return *this;

	for (uint i = 0; i < addrsLength; i++)
		*(objAddr[i]) %= data;

	return *this;
}

template <class DataType>
const typename CustomArray<DataType>::addrIDX& CustomArray<DataType>::addrIDX::operator>>= (const DataType& data) const
{
	if (!objAddr)
		return *this;

	for (uint i = 0; i < addrsLength; i++)
		*(objAddr[i]) >>= data;

	return *this;
}

template <class DataType>
const typename CustomArray<DataType>::addrIDX& CustomArray<DataType>::addrIDX::operator<<= (const DataType& data) const
{
	if (!objAddr)
		return *this;

	for (uint i = 0; i < addrsLength; i++)
		*(objAddr[i]) <<= data;

	return *this;
}

template <class DataType>
typename CustomArray<DataType>::uint CustomArray<DataType>::addrIDX::length() const
{
	return addrsLength;
}
/*---------------------------------------------------------------------------------------------------------------------------------*/

template <class DataType>
/// <summary>default constructor</summary>
CustomArray<DataType>::CustomArray() : allocAddr(nullptr), allocLength(0), allocSize(0)
{

}

template <class DataType>
/// <summary>construct an array of 'newLength' objects</summary>
CustomArray<DataType>::CustomArray(uint newLength) : CustomArray<DataType>()
{
	resize(newLength);
}

template <class DataType>
/// <summary>copy constructor</summary>
CustomArray<DataType>::CustomArray(const CustomArray<DataType>& csArr) : CustomArray<DataType>()
{
	copyFrom(csArr);
}

template <class DataType>
/// <summary>move constructor</summary>
CustomArray<DataType>::CustomArray(CustomArray<DataType>&& csArr) : allocAddr(csArr.allocAddr), allocLength(csArr.allocLength), allocSize(csArr.allocSize)
{
	csArr.allocAddr = nullptr;
	csArr.allocLength = 0;
	csArr.allocSize = 0;
}

template <class DataType>
/// <summary>construct an array from an initializer_list</summary>
CustomArray<DataType>::CustomArray(const std::initializer_list<DataType>& initList) : CustomArray<DataType>()
{
	const uint listLength = initList.size();
	const DataType* listAddr = initList.begin();

	if (!listLength)
		return;

	if (!resize(listLength))
		return;

	for (uint i = 0; i < listLength; i++)
		allocAddr[i] = listAddr[i];
}

template <class DataType>
/// <summary>destructor</summary>
CustomArray<DataType>::~CustomArray()
{
	clear();
}

template <class DataType>
/// <summary>resize current array to 'newLength' objects</summary>
bool CustomArray<DataType>::resize(uint newLength)
{
	if (newLength == allocLength) // if same size
		return true;
	else if (!newLength) // if zero-lengthed array
	{
		clear();
		return true;
	} 

	if (!allocAddr) // if empty array
	{
		DataType* allocAddr_ = (DataType*)calloc(newLength, sizeof(DataType)); // request a new mem block
		if (allocAddr_) // allocation succeeded
		{
			// instantiate each object
			for (uint i = 0; i < newLength; i++)
				new(allocAddr_ + i) DataType;

			// update allocation info
			allocAddr = allocAddr_;
			allocLength = newLength;
			allocSize = newLength * sizeof(DataType);

			return true;
		}
		else // allocation failed
			return false;
	}
	else // if already allocated array
	{
		if (newLength > allocLength) // if adding new elemnts
		{
			DataType* allocAddr_ = (DataType*)realloc(allocAddr, newLength * sizeof(DataType));
			if (allocAddr_) // resizing succeeded
			{
				// zero-out newly allocated memory
				for (uint i = 0; i < newLength * sizeof(DataType) - allocSize; i++)
					*((char*)(allocAddr_ + allocLength) + i) = 0; // address of (last element + 1) + (byte offset)

				// instantiate newly allocated objects
				for (uint i = 0; i < newLength - allocLength; i++)
					new(allocAddr_ + allocLength + i) DataType;

				// update allocation info
				allocAddr = allocAddr_;
				allocLength = newLength;
				allocSize = newLength * sizeof(DataType);

				return true;
			}
			else // resizing failed
				return false;
		}
		else // if removing elemnts
		{
			// destruct objects from the end
			for (uint i = 0; i < allocLength - newLength; i++)
				allocAddr[allocLength - 1 - i].~DataType();

			DataType* allocAddr_ = (DataType*)realloc(allocAddr, newLength * sizeof(DataType));
			if (allocAddr_) // resizing succeeded (unlikely to fail because newLength < allocLength)
			{
				// update allocation info
				allocAddr = allocAddr_;
				allocLength = newLength;
				allocSize = newLength * sizeof(DataType);

				return true;
			}
			else // resizing failed
				return false;
		}
	}
}

template <class DataType>
/// <summary>clear the current array (remove all objects)</summary>
void CustomArray<DataType>::clear()
{
	if (allocAddr) // if already allocated array
	{
		// destruct objects from the end
		for (uint i = 0; i < allocLength; i++)
			allocAddr[allocLength - 1 - i].~DataType();

		// free mem block
		free(allocAddr);

		// clear allocation info
		clearAllocInfo();
	}
}

template <class DataType>
/// <summary>get or set an object whose index is 'location' (zero based)</summary>
DataType& CustomArray<DataType>::operator[] (uint location) const
{
	assert(location < allocLength);

	return allocAddr[location];
}

template <class DataType>
/// <summary>get or set an object whose index is 'location' (zero based)</summary>
DataType& CustomArray<DataType>::at(uint location) const
{
	assert(location < allocLength);

	return allocAddr[location];
}

template <class DataType>
/// <summary>get or set an object whose index is 'location' (zero based)</summary>
DataType& CustomArray<DataType>::operator() (uint location) const
{
	assert(location < allocLength);

	return allocAddr[location];
}

template <class DataType>
/// <summary>assign 'data' to all objects of the array</summary>
CustomArray<DataType>& CustomArray<DataType>::operator= (const DataType& data)
{
	// resize to one object array
	if (!resize(1))
		return *this;

	// assign data
	allocAddr[0] = data;
	
	return *this;
}

template <class DataType>
/// <summary>copy objects from another CustomArray</summary>
CustomArray<DataType>& CustomArray<DataType>::operator= (const CustomArray<DataType>& csArr)
{
	// resize to the length of 'csArr' (source) array
	if (!resize(csArr.allocLength))
		return *this;

	// copy data
	copyFrom(csArr);

	return *this;
}

template <class DataType>
/// <summary>move objects from another CustomArray</summary>
CustomArray<DataType>& CustomArray<DataType>::operator= (CustomArray<DataType>&& csArr)
{
	allocAddr = csArr.allocAddr;
	allocLength = csArr.allocLength;
	allocSize = csArr.allocSize;

	csArr.allocAddr = nullptr;
	csArr.allocLength = 0;
	csArr.allocSize = 0;

	return *this;
}

template <class DataType>
/// <summary>copy objects from another CustomArray</summary>
CustomArray<DataType>& CustomArray<DataType>::operator= (const std::initializer_list<DataType>& initList)
{
	const uint listLength = initList.size();
	const DataType* listAddr = initList.begin();

	if (!listLength) // empty array
	{
		clear();
		return *this;
	}

	if (!resize(listLength))
		return *this;

	for (uint i = 0; i < listLength; i++)
		allocAddr[i] = listAddr[i];
}

template <class DataType>
/// <summary>assign (by copying) all array objects to 'data'</summary>
void CustomArray<DataType>::fillWith(const DataType& data) const
{
	for (uint i = 0; i < allocLength; i++)
		allocAddr[i] = data;
}

template <class DataType>
/// <summary>copy all objects of another CustomArray to current one</summary>
bool CustomArray<DataType>::copyFrom(const CustomArray<DataType>& csArr, uint srcStartIdx = 0, uint destStartIdx = 0)
{
	// return if source array is not allocated
	if (!(csArr.allocAddr))
		return false;

	if (srcStartIdx >= csArr.allocLength) // error index out of bound
		return false;

	// calc length of required objects
	const uint requiredLength = (csArr.allocLength - srcStartIdx) + destStartIdx; // no. of objects from source array + start offset

	// allocate memory block if current array is unallocated or it's length is smaller than required
	if ((!allocAddr) || (allocLength < requiredLength))
	{
		if (!resize(requiredLength))
			return false;
	}

	// copy objects
	for (uint i = 0; i < csArr.allocLength - srcStartIdx; i++)
		allocAddr[destStartIdx + i] = csArr.allocAddr[srcStartIdx + i];

	return true;
}

template <class DataType>
/// <summary>copy all objects from current array to another CustomArray</summary>
bool CustomArray<DataType>::copyTo(CustomArray<DataType>& csArr, uint srcStartIdx = 0, uint destStartIdx = 0) const
{
	return csArr.copyFrom(*this, srcStartIdx, destStartIdx);
}

template <class DataType>
/// <summary>puts an object at the end of the array</summary>
bool CustomArray<DataType>::push(const DataType& data)
{
	if (!resize(allocLength + 1))
		return false;

	allocAddr[allocLength - 1] = data;

	return true;
}

template <class DataType>
/// <summary>remove the last object from the array</summary>
bool CustomArray<DataType>::pop()
{
	if (!resize(allocLength - 1))
		return false;

	return true;
}

template <class DataType>
/// <summary>get the length of the array</summary>
typename CustomArray<DataType>::uint CustomArray<DataType>::length() const
{
	return allocLength;
}

template <class DataType>
/// <summary>get the size of the allocated memory (in bytes)</summary>
typename CustomArray<DataType>::uint CustomArray<DataType>::memSize() const
{
	return allocSize + sizeof(CustomArray);
}

template <class DataType>
/// <summary>get all objects that satisfy a condition</summary>
typename CustomArray<DataType>::addrIDX CustomArray<DataType>::operator() (const boolIDX& objFlag_) const
{
	if (objFlag_.flagsLength != allocLength)
		return addrIDX();

	addrIDX objAddr_(objFlag_.length()); // .length() returns number of 1's

	for (uint i = 0, j = 0; i < allocLength; i++)
	{
		if (objFlag_.objFlag[i])
			objAddr_.objAddr[j++] = allocAddr + i;
	}

	return objAddr_;
}

template <class DataType>
/// <summary>get all objects whose indices are indicated by 'initList'</summary>
typename CustomArray<DataType>::addrIDX CustomArray<DataType>::operator() (const std::initializer_list<uint>& initList) const
{
	const uint listLength = initList.size();
	const uint* listAddr = initList.begin();

	if (!allocAddr || !listLength)
		return addrIDX();

	// check if out of bound index
	for (uint i = 0; i < listLength; i++)
	{
		if (listAddr[i] >= allocLength)
			return addrIDX();
	}

	addrIDX objAddr_(listLength);

	// assign addresses to 'objAddr_'
	for (uint i = 0; i < listLength; i++)
		objAddr_.objAddr[i] = allocAddr + listAddr[i];

	return objAddr_;
}

template <class DataType>
typename CustomArray<DataType>::boolIDX CustomArray<DataType>::operator< (const DataType& data) const
{
	boolIDX objFlag_(allocLength);

	for (uint i = 0; i < allocLength; i++)
	{
		if (allocAddr[i] < data)
			objFlag_.objFlag[i] = true;
	}

	return objFlag_;
}

template <class DataType>
typename CustomArray<DataType>::boolIDX CustomArray<DataType>::operator<= (const DataType& data) const
{
	boolIDX objFlag_(allocLength);

	for (uint i = 0; i < allocLength; i++)
	{
		if (allocAddr[i] <= data)
			objFlag_.objFlag[i] = true;
	}

	return objFlag_;
}

template <class DataType>
typename CustomArray<DataType>::boolIDX CustomArray<DataType>::operator>(const DataType& data) const
{
	boolIDX objFlag_(allocLength);

	for (uint i = 0; i < allocLength; i++)
	{
		if (allocAddr[i] > data)
			objFlag_.objFlag[i] = true;
	}

	return objFlag_;
}

template <class DataType>
typename CustomArray<DataType>::boolIDX CustomArray<DataType>::operator>= (const DataType& data) const
{
	boolIDX objFlag_(allocLength);

	for (uint i = 0; i < allocLength; i++)
	{
		if (allocAddr[i] >= data)
			objFlag_.objFlag[i] = true;
	}

	return objFlag_;
}

template <class DataType>
typename CustomArray<DataType>::boolIDX CustomArray<DataType>::operator== (const DataType& data) const
{
	boolIDX objFlag_(allocLength);

	for (uint i = 0; i < allocLength; i++)
	{
		if (allocAddr[i] == data)
			objFlag_.objFlag[i] = true;
	}

	return objFlag_;
}

template <class DataType>
typename CustomArray<DataType>::boolIDX CustomArray<DataType>::operator!= (const DataType& data) const
{
	boolIDX objFlag_(allocLength);

	for (uint i = 0; i < allocLength; i++)
	{
		if (allocAddr[i] != data)
			objFlag_.objFlag[i] = true;
	}

	return objFlag_;
}


#endif
